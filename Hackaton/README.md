# Hackaton-VemSerDBC 2021 #

##  Serviço de leitura de Dados ##

### Qual é a função desse projeto? ###

* O presente projeto é um sistema de leitura de dados utilzando-se de spring batch.
* O sistema de análise de dados, importa lotes de arquivos .dat e realiza a leitura e analise dos dados contindos nos arquivos para então produzir um relatório desse conteúdo.
* Existem 3 tipos de dados dentro desses arquivos. Para cada tipo de dados há um
layout diferente.
* Version 1.0v

## Funcionamento do sitema ##

* O sistema lê dados do diretório padrão, localizado em% HOMEPATH% /
data / in.
* O sistema lê somente arquivos .dat.
* Depois de processar todos os arquivos dentro do diretório padrão de entrada, o
sistema cria um arquivo dentro do diretório de saída padrão, localizado em%
HOMEPATH% /data/out.
* O nome do arquivo segue o padrão, {flat_file_name} .done.dat.
* O conteúdo do arquivo de saída é constituido dos seguintes dados:
- Quantidade de clientes no arquivo de entrada
- Quantidade de vendedor no arquivo de entrada
- ID da venda mais cara
- O pior vendedor
* Após inicializado o sistema permanece sempre ativo.



## Feramentas Utilizadas

- [IntelliJ IDEA](https://www.jetbrains.com/pt-br/idea/)
- [Docker](https://www.docker.com/products/docker-desktop)
- [BitBucket](https://bitbucket.org/)



### Dependencias ###

- [Spring Batch](https://spring.io/projects/spring-batch)



### Autores

- Artur Branco
- Gustavo Wolff
- Iago Duarte
- Marco Antônio M. Roos




