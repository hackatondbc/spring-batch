package br.com.dbccompany.Hackaton.TestesUnitarios.DTO;

import static org.junit.Assert.*;

import br.com.dbccompany.Hackaton.DTO.ClienteDTO;
import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class ClienteTest {



    @Test
    public void criarClienteEBuscarCampos(){
        ClienteDTO cliente = new ClienteDTO();
        cliente.setCnpj("223454654-2442333-3");
        cliente.setNome("Jorge");
        cliente.setAreaNegocio("Barman");
        Assertions.assertEquals( "Jorge", cliente.getNome() );
        Assertions.assertEquals( "223454654-2442333-3", cliente.getCnpj() );
        Assertions.assertEquals( "Barman", cliente.getAreaNegocio() );
    }
}
