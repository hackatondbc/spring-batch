package br.com.dbccompany.Hackaton.TestesUnitarios.DTO;

import br.com.dbccompany.Hackaton.DTO.VendedorDTO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class VendedorTest {

    @Test
    public void criarEAcessarParametros(){
        VendedorDTO vendedor = new VendedorDTO();
        vendedor.setCpf("444.232.455-44");
        vendedor.setNome("Ricardo");
        vendedor.setSalario("4432");
        Assertions.assertEquals("444.232.455-44", vendedor.getCpf());
        Assertions.assertEquals("Ricardo", vendedor.getNome());
        Assertions.assertEquals("4432", vendedor.getSalario());
    }
}
