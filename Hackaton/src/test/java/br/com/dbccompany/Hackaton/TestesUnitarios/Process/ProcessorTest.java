package br.com.dbccompany.Hackaton.TestesUnitarios.Process;

import br.com.dbccompany.Hackaton.DTO.ClienteDTO;
import br.com.dbccompany.Hackaton.DTO.VendaDTO;
import br.com.dbccompany.Hackaton.DTO.VendedorDTO;
import br.com.dbccompany.Hackaton.DTO.WrapperDTO;
import br.com.dbccompany.Hackaton.Exception.ClienteComCamposInvalidos;
import br.com.dbccompany.Hackaton.Exception.IdentificadorInvalido;
import br.com.dbccompany.Hackaton.Exception.VendaComCamposInvalidos;
import br.com.dbccompany.Hackaton.Exception.VendedorComCamposInvalidos;
import br.com.dbccompany.Hackaton.IO.RawData;
import br.com.dbccompany.Hackaton.Process.ProcessorService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class ProcessorTest {

    @Test
    public void converterStringEmArrayList(){
        ProcessorService service = new ProcessorService();
        String string = "aaassdisjdfijgt44e =-09s9sç4wfv-,09=wvm,59by-ue5";

        List<String> resultado = service.converterStringEmList(string, "=");
        List<String> esperado = new ArrayList<>();
        String um = "aaassdisjdfijgt44e ";
        String dois = "-09s9sç4wfv-,09";
        String tres = "wvm,59by-ue5";
        esperado.add(um);
        esperado.add(dois);
        esperado.add(tres);
        Assertions.assertEquals(esperado, resultado);
    }

    @Test
    public void dividirPorEspacoEZerosEmArray(){
        ProcessorService service = new ProcessorService();
        String string = "001ç1234567891234çPedroç50000 001ç3245678865434çPauloç40000.99 002ç2345675434544345çJose da SilvaçRural 002ç2345675433444345çEduardo PereiraçRural";
        RawData dado = new RawData();
        dado.setTexto(string);
        List<String> resultado = service.dividirPorEspacoEZerosEmArray(dado);
        List<String> esperado = new ArrayList<>();
        String valor1 = "1ç1234567891234çPedroç50000";
        String valor2 = "1ç3245678865434çPauloç40000.99";
        String valor3 = "2ç2345675434544345çJose da SilvaçRural";
        String valor4 = "2ç2345675433444345çEduardo PereiraçRural";
        esperado.add(valor1);
        esperado.add(valor2);
        esperado.add(valor3);
        esperado.add(valor4);

        Assertions.assertEquals(esperado, resultado);
    }

    @Test
    public void construirCliente() throws ClienteComCamposInvalidos {
        ProcessorService processor = new ProcessorService();
        String preCliente = "2ç2345675434544345çJose da SilvaçRural";
        ClienteDTO cliente = processor.construirCliente( preCliente );
        ClienteDTO esperado = new ClienteDTO( "2345675434544345", "Jose da Silva", "Rural" );

        Assertions.assertEquals( cliente.getCnpj(), esperado.getCnpj() );
        Assertions.assertEquals( cliente.getNome(), esperado.getNome() );
        Assertions.assertEquals( cliente.getAreaNegocio(), esperado.getAreaNegocio() );
    }

    @Test
    public void construirVendedor() throws VendedorComCamposInvalidos {
        ProcessorService processor = new ProcessorService();
        String preVendedor = "1ç1234567891234çPedroç50000";
        VendedorDTO resultado = processor.construirVendedor(preVendedor);
        VendedorDTO esperado = new VendedorDTO("1234567891234", "Pedro", "50000");
        Assertions.assertEquals(esperado.getNome(), resultado.getNome() );
        Assertions.assertEquals(esperado.getCpf(), resultado.getCpf() );
        Assertions.assertEquals(esperado.getSalario(), resultado.getSalario() );
    }

    @Test
    public void construirVenda() throws VendaComCamposInvalidos {
        ProcessorService processor = new ProcessorService();
        String preVenda = "3ç10ç[1-10-100,2-30-2.50,3-40-3.10]çPedro";

        WrapperDTO wrapper = new WrapperDTO();
        List<VendedorDTO> vendedores = new ArrayList<>();
        VendedorDTO vendedor = new VendedorDTO("1234567891234", "Pedro", "50000");
        vendedores.add(vendedor);
        wrapper.setVendedores(vendedores);

        VendaDTO resultado = processor.construirVenda( preVenda, wrapper );

        Assertions.assertEquals("10", resultado.getId());
        Assertions.assertEquals("50000", resultado.getVendedor().getSalario());
        Assertions.assertEquals("2.50", resultado.getItens().get(1).getPreco());
    }

    @Test
    public void construirWrapper() throws IdentificadorInvalido {
        ProcessorService processor = new ProcessorService();
        String texto = "001ç1234567891234çPedroç50000 001ç3245678865434çPauloç40000.99 002ç2345675434544345çJose da SilvaçRural 002ç2345675433444345çEduardo PereiraçRural 003ç10ç[1-10-100,2-30-2.50,3-40-3.10]çPedro 003ç08ç[1-34-10,2-33-1.50,3-40-0.10]çPaulo";
        RawData dado = new RawData( texto );
        List<String> dadoFormatado = processor.dividirPorEspacoEZerosEmArray( dado );
        WrapperDTO wrapper = processor.construirWrapper( dadoFormatado );

        Assertions.assertEquals(2, wrapper.getVendas().size());
        Assertions.assertEquals( "3.10", wrapper.getVendas().get(0).getItens().get(2).getPreco() );


    }
}
