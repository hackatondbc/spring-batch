package br.com.dbccompany.Hackaton.TestesUnitarios.DTO;

import br.com.dbccompany.Hackaton.DTO.ClienteDTO;
import br.com.dbccompany.Hackaton.DTO.ItemDTO;
import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ItemTest {

    @Test
    public void salvarItemEBuscarPropriedades(){
        ItemDTO item = new ItemDTO();
        item.setId("11");
        item.setPreco("333.50");
        item.setQuantidade("20");
        Assertions.assertEquals("11", item.getId());
        Assertions.assertEquals("333.50", item.getPreco());
        Assertions.assertEquals("20", item.getQuantidade());
    }
}
