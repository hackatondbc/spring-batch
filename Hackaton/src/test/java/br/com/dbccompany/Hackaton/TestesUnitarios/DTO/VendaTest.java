package br.com.dbccompany.Hackaton.TestesUnitarios.DTO;

import br.com.dbccompany.Hackaton.DTO.ItemDTO;
import br.com.dbccompany.Hackaton.DTO.VendaDTO;
import br.com.dbccompany.Hackaton.DTO.VendedorDTO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class VendaTest {

    @Test
    public void criarVendaEBuscarCampos(){
        VendaDTO venda = new VendaDTO();
        venda.setId("11");
        List<ItemDTO> itens = new ArrayList<>();
        itens.add(new ItemDTO("11", "4", "10"));
        itens.add(new ItemDTO("1", "123", "15"));
        itens.add(new ItemDTO("2", "765", "20"));
        itens.add(new ItemDTO("5", "554", "25"));
        itens.add(new ItemDTO("9", "22", "30"));
        venda.setItens(itens);
        VendedorDTO vendedor = new VendedorDTO("444.323.122-54", "Paulo", "40500");
        venda.setVendedor(vendedor);
        Assertions.assertEquals("11", venda.getId());
        Assertions.assertEquals(5, venda.getItens().size());
        Assertions.assertEquals("765", venda.getItens().get(2).getPreco());
        Assertions.assertEquals(vendedor, venda.getVendedor());
    }
}
