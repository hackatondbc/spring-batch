package br.com.dbccompany.Hackaton.TestesUnitarios.Util;

import br.com.dbccompany.Hackaton.DTO.*;
import br.com.dbccompany.Hackaton.Process.ProcessorService;
import br.com.dbccompany.Hackaton.Util.AnaliseDeDados;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class AnaliseDeDadosTest {

    @Test
    public void quantidadelientes(){
        WrapperDTO wrapper = new WrapperDTO();

        ClienteDTO cliente = new ClienteDTO();
        cliente.setCnpj("44444-444-3");
        cliente.setNome("Pedro");
        cliente.setAreaNegocio("Mergulhador");
        ClienteDTO cliente2 = new ClienteDTO();
        cliente2.setCnpj("223454654-2442333-3");
        cliente2.setNome("Jorge");
        cliente2.setAreaNegocio("Barman");
        List<ClienteDTO> clientes = new ArrayList<>();
        clientes.add(cliente);
        clientes.add(cliente2);
        wrapper.setClientes(clientes);

        VendedorDTO vendedor = new VendedorDTO();
        vendedor.setCpf("444.232.455-44");
        vendedor.setNome("Ricardo");
        vendedor.setSalario("4432");
        VendedorDTO vendedor2 = new VendedorDTO();
        vendedor2.setCpf("555.343.6543-56");
        vendedor2.setNome("Gabriel");
        vendedor2.setSalario("10500");
        List<VendedorDTO> vendedores = new ArrayList<>();
        vendedores.add(vendedor);
        vendedores.add(vendedor2);
        wrapper.setVendedores(vendedores);

        VendaDTO venda = new VendaDTO();
        venda.setId("11");
        List<ItemDTO> itens = new ArrayList<>();
        itens.add(new ItemDTO("11", "4", "10"));
        itens.add(new ItemDTO("1", "123", "15"));
        itens.add(new ItemDTO("2", "765", "20"));
        itens.add(new ItemDTO("5", "554", "25"));
        itens.add(new ItemDTO("9", "22", "30"));
        venda.setItens(itens);
        venda.setVendedor(vendedor);
        VendaDTO venda2 = new VendaDTO();
        venda2.setId("11");
        List<ItemDTO> itens2 = new ArrayList<>();
        itens.add(new ItemDTO("4", "34", "3"));
        itens.add(new ItemDTO("2", "345", "4"));
        itens.add(new ItemDTO("7", "65", "5"));
        itens.add(new ItemDTO("8", "66", "7"));
        itens.add(new ItemDTO("2", "43", "8"));
        venda2.setItens(itens);
        venda2.setVendedor(vendedor2);
        List<VendaDTO> vendas = new ArrayList<>();
        vendas.add(venda);
        vendas.add(venda2);
        wrapper.setVendas(vendas);

        AnaliseDeDados analisador = new AnaliseDeDados( wrapper );
        String resultado = analisador.quantidadeClientes();
        Assertions.assertEquals("2", resultado);
    }

    @Test
    public void quantidadeVendedores(){
        WrapperDTO wrapper = new WrapperDTO();

        ClienteDTO cliente = new ClienteDTO();
        cliente.setCnpj("44444-444-3");
        cliente.setNome("Pedro");
        cliente.setAreaNegocio("Mergulhador");
        ClienteDTO cliente2 = new ClienteDTO();
        cliente2.setCnpj("223454654-2442333-3");
        cliente2.setNome("Jorge");
        cliente2.setAreaNegocio("Barman");
        List<ClienteDTO> clientes = new ArrayList<>();
        clientes.add(cliente);
        clientes.add(cliente2);
        wrapper.setClientes(clientes);

        VendedorDTO vendedor = new VendedorDTO();
        vendedor.setCpf("444.232.455-44");
        vendedor.setNome("Ricardo");
        vendedor.setSalario("4432");
        VendedorDTO vendedor2 = new VendedorDTO();
        vendedor2.setCpf("555.343.6543-56");
        vendedor2.setNome("Gabriel");
        vendedor2.setSalario("10500");
        List<VendedorDTO> vendedores = new ArrayList<>();
        vendedores.add(vendedor);
        vendedores.add(vendedor2);
        wrapper.setVendedores(vendedores);

        VendaDTO venda = new VendaDTO();
        venda.setId("11");
        List<ItemDTO> itens = new ArrayList<>();
        itens.add(new ItemDTO("11", "4", "10"));
        itens.add(new ItemDTO("1", "123", "15"));
        itens.add(new ItemDTO("2", "765", "20"));
        itens.add(new ItemDTO("5", "554", "25"));
        itens.add(new ItemDTO("9", "22", "30"));
        venda.setItens(itens);
        venda.setVendedor(vendedor);
        VendaDTO venda2 = new VendaDTO();
        venda2.setId("11");
        List<ItemDTO> itens2 = new ArrayList<>();
        itens.add(new ItemDTO("4", "34", "3"));
        itens.add(new ItemDTO("2", "345", "4"));
        itens.add(new ItemDTO("7", "65", "5"));
        itens.add(new ItemDTO("8", "66", "7"));
        itens.add(new ItemDTO("2", "43", "8"));
        venda2.setItens(itens);
        venda2.setVendedor(vendedor2);
        List<VendaDTO> vendas = new ArrayList<>();
        vendas.add(venda);
        vendas.add(venda2);
        wrapper.setVendas(vendas);

        AnaliseDeDados analisador = new AnaliseDeDados( wrapper );
        String resultado = analisador.quantidadeVendedores();
        Assertions.assertEquals("2", resultado);
    }

    @Test
    public void idVendaMaisCara(){
        WrapperDTO wrapper = new WrapperDTO();

        ClienteDTO cliente = new ClienteDTO();
        cliente.setCnpj("44444-444-3");
        cliente.setNome("Pedro");
        cliente.setAreaNegocio("Mergulhador");
        ClienteDTO cliente2 = new ClienteDTO();
        cliente2.setCnpj("223454654-2442333-3");
        cliente2.setNome("Jorge");
        cliente2.setAreaNegocio("Barman");
        List<ClienteDTO> clientes = new ArrayList<>();
        clientes.add(cliente);
        clientes.add(cliente2);
        wrapper.setClientes(clientes);

        VendedorDTO vendedor = new VendedorDTO();
        vendedor.setCpf("444.232.455-44");
        vendedor.setNome("Ricardo");
        vendedor.setSalario("4432");
        VendedorDTO vendedor2 = new VendedorDTO();
        vendedor2.setCpf("555.343.6543-56");
        vendedor2.setNome("Gabriel");
        vendedor2.setSalario("10500");
        List<VendedorDTO> vendedores = new ArrayList<>();
        vendedores.add(vendedor);
        vendedores.add(vendedor2);
        wrapper.setVendedores(vendedores);

        VendaDTO venda = new VendaDTO();
        venda.setId("11");
        List<ItemDTO> itens = new ArrayList<>();
        itens.add(new ItemDTO("11", "4", "1"));
        itens.add(new ItemDTO("1", "123", "1"));
        itens.add(new ItemDTO("2", "765", "1"));
        itens.add(new ItemDTO("5", "554", "1"));
        itens.add(new ItemDTO("9", "22", "1"));
        venda.setItens(itens);
        venda.setVendedor(vendedor);
        VendaDTO venda2 = new VendaDTO();
        venda2.setId("1");
        List<ItemDTO> itens2 = new ArrayList<>();
        itens2.add(new ItemDTO("4", "34", "1"));
        itens2.add(new ItemDTO("2", "345", "1"));
        itens2.add(new ItemDTO("7", "65", "1"));
        itens2.add(new ItemDTO("8", "66", "1"));
        itens2.add(new ItemDTO("2", "43", "1"));
        venda2.setItens(itens2);
        venda2.setVendedor(vendedor2);
        VendaDTO venda3 = new VendaDTO();
        venda3.setId("12");
        List<ItemDTO> itens3 = new ArrayList<>();
        itens3.add(new ItemDTO("4", "34", "1"));
        itens3.add(new ItemDTO("2", "345", "1"));
        itens3.add(new ItemDTO("7", "65", "1"));
        itens3.add(new ItemDTO("8", "66", "1"));
        itens3.add(new ItemDTO("2", "43", "1"));
        venda3.setItens(itens3);
        venda3.setVendedor(vendedor2);
        List<VendaDTO> vendas = new ArrayList<>();
        vendas.add(venda);
        vendas.add(venda2);
        vendas.add(venda3);
        wrapper.setVendas(vendas);

        AnaliseDeDados analisador = new AnaliseDeDados( wrapper );
        String resultado = analisador.idVendaMaisCara();

        Assertions.assertEquals("11", resultado);
    }

    @Test
    public void piorVendedor(){
        WrapperDTO wrapper = new WrapperDTO();

        ClienteDTO cliente = new ClienteDTO();
        cliente.setCnpj("44444-444-3");
        cliente.setNome("Pedro");
        cliente.setAreaNegocio("Mergulhador");
        ClienteDTO cliente2 = new ClienteDTO();
        cliente2.setCnpj("223454654-2442333-3");
        cliente2.setNome("Jorge");
        cliente2.setAreaNegocio("Barman");
        List<ClienteDTO> clientes = new ArrayList<>();
        clientes.add(cliente);
        clientes.add(cliente2);
        wrapper.setClientes(clientes);

        VendedorDTO vendedor = new VendedorDTO();
        vendedor.setCpf("444.232.455-44");
        vendedor.setNome("Ricardo");
        vendedor.setSalario("4432");
        VendedorDTO vendedor2 = new VendedorDTO();
        vendedor2.setCpf("555.343.6543-56");
        vendedor2.setNome("Gabriel");
        vendedor2.setSalario("10500");
        List<VendedorDTO> vendedores = new ArrayList<>();
        vendedores.add(vendedor);
        vendedores.add(vendedor2);
        wrapper.setVendedores(vendedores);

        VendaDTO venda = new VendaDTO();
        venda.setId("11");
        List<ItemDTO> itens = new ArrayList<>();
        itens.add(new ItemDTO("11", "4", "1"));
        itens.add(new ItemDTO("1", "123", "1"));
        itens.add(new ItemDTO("2", "765", "1"));
        itens.add(new ItemDTO("5", "554", "1"));
        itens.add(new ItemDTO("9", "22", "1"));
        venda.setItens(itens);
        venda.setVendedor(vendedor);
        VendaDTO venda2 = new VendaDTO();
        venda2.setId("1");
        List<ItemDTO> itens2 = new ArrayList<>();
        itens2.add(new ItemDTO("4", "34", "1"));
        itens2.add(new ItemDTO("2", "345", "1"));
        itens2.add(new ItemDTO("7", "65", "1"));
        itens2.add(new ItemDTO("8", "66", "1"));
        itens2.add(new ItemDTO("2", "43", "1"));
        venda2.setItens(itens2);
        venda2.setVendedor(vendedor2);
        VendaDTO venda3 = new VendaDTO();
        venda3.setId("12");
        List<ItemDTO> itens3 = new ArrayList<>();
        itens3.add(new ItemDTO("4", "34", "1"));
        itens3.add(new ItemDTO("2", "345", "1"));
        itens3.add(new ItemDTO("7", "65", "1"));
        itens3.add(new ItemDTO("8", "66", "1"));
        itens3.add(new ItemDTO("2", "43", "1"));
        venda3.setItens(itens3);
        venda3.setVendedor(vendedor2);
        List<VendaDTO> vendas = new ArrayList<>();
        vendas.add(venda);
        vendas.add(venda2);
        vendas.add(venda3);
        wrapper.setVendas(vendas);

        AnaliseDeDados analisador = new AnaliseDeDados( wrapper );
        String resultado = analisador.piorVendedor();
        Assertions.assertEquals("cpf='555.343.6543-56', nome='Gabriel', salario=10500", resultado);

    }
}
