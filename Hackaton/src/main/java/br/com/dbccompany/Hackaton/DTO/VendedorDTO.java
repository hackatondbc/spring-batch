package br.com.dbccompany.Hackaton.DTO;

public class VendedorDTO {
    private String cpf;
    private String nome;
    private String salario;

    public VendedorDTO(String cpf, String nome, String salario) {
        this.cpf = cpf;
        this.nome = nome;
        this.salario = salario;
    }

    public VendedorDTO() {
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSalario() {
        return salario;
    }

    public void setSalario(String salario) {
        this.salario = salario;
    }

    @Override
    public String toString() {
        return  "cpf='" + cpf + '\'' +
                ", nome='" + nome + '\'' +
                ", salario=" + salario;
    }
}
