package br.com.dbccompany.Hackaton.Exception;

public class VendaComCamposInvalidos extends GeneralException{
    public VendaComCamposInvalidos(){
        super("Pré venda possui campos inválidos!");
    }
}
