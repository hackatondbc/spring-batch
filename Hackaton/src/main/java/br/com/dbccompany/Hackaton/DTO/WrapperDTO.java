package br.com.dbccompany.Hackaton.DTO;

import java.util.ArrayList;
import java.util.List;

public class WrapperDTO {
    private List<VendedorDTO> vendedores = new ArrayList<>();
    private List<ClienteDTO> clientes = new ArrayList<>();
    private List<VendaDTO> vendas = new ArrayList<>();

    public WrapperDTO(List<VendedorDTO> vendedores, List<ClienteDTO> clientes, List<VendaDTO> vendas) {
        this.vendedores = vendedores;
        this.clientes = clientes;
        this.vendas = vendas;
    }

    public WrapperDTO() {
    }

    public List<VendaDTO> procurarVendasDeUmVendedor( String nome ){
        List<VendaDTO> listaVendas = new ArrayList<>();
        for( VendaDTO venda : this.vendas ){
            if(venda.getVendedor().getNome().equals(nome)){
                listaVendas.add(venda);
            }
        }
        return listaVendas;
    }

    public VendedorDTO procurarVendedorPorNome( String nome ){
        for( VendedorDTO vendedor : this.vendedores ){
            if( vendedor.getNome().equals(nome) ){
                return vendedor;
            }
        }
        return null;
    }

    public void adicionarCliente( ClienteDTO cliente ){
        this.clientes.add(cliente);
    }

    public void adicionarVendedor( VendedorDTO vendedor ){
        this.vendedores.add(vendedor);
    }

    public void adicionarVendas(VendaDTO venda){
        this.vendas.add(venda);
    }

    public List<VendedorDTO> getVendedores() {
        return vendedores;
    }

    public void setVendedores(List<VendedorDTO> vendedores) {
        this.vendedores = vendedores;
    }

    public List<ClienteDTO> getClientes() {
        return clientes;
    }

    public void setClientes(List<ClienteDTO> clientes) {
        this.clientes = clientes;
    }

    public List<VendaDTO> getVendas() {
        return vendas;
    }

    public void setVendas(List<VendaDTO> vendas) {
        this.vendas = vendas;
    }

}
