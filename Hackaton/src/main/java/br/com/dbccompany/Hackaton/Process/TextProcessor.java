package br.com.dbccompany.Hackaton.Process;

import br.com.dbccompany.Hackaton.Exception.GeneralException;
import br.com.dbccompany.Hackaton.IO.RawData;
import br.com.dbccompany.Hackaton.Util.LogsGenerator;
import org.springframework.batch.item.ItemProcessor;

public class TextProcessor implements ItemProcessor<RawData, RawData> {

    @Override
    public RawData process(RawData texto ){
        try{
            ProcessorService service = new ProcessorService();
            return service.processadorDeArquivos(texto);
        } catch( GeneralException e ){
            LogsGenerator.erro( e );
            return null;
        }
    }
}
