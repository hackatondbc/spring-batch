package br.com.dbccompany.Hackaton.DTO;

public class ClienteDTO {
    private String cnpj;
    private String nome;
    private String areaNegocio;

    public ClienteDTO(String cnpj, String nome, String areaNegocio) {
        this.cnpj = cnpj;
        this.nome = nome;
        this.areaNegocio = areaNegocio;
    }

    public ClienteDTO() {
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getAreaNegocio() {
        return areaNegocio;
    }

    public void setAreaNegocio(String areaNegocio) {
        this.areaNegocio = areaNegocio;
    }
}
