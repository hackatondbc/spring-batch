package br.com.dbccompany.Hackaton.Exception;

public class DadosAnalisadosInvalidos extends GeneralException{
    public DadosAnalisadosInvalidos(){
        super("Analisador recebeu dados inválidos!");
    }
}
