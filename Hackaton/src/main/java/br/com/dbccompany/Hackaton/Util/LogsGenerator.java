package br.com.dbccompany.Hackaton.Util;

import br.com.dbccompany.Hackaton.DTO.ErroDTO;
import br.com.dbccompany.Hackaton.Exception.GeneralException;
import br.com.dbccompany.Hackaton.HackatonApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;

public class LogsGenerator {

    public static Logger logger = LoggerFactory.getLogger(HackatonApplication.class);

    public static RestTemplate restTemplate = new RestTemplate();

    public static String url = "http://localhost:8081/hackatonLogs/salvar";

    public static void erro (GeneralException e){
        logger.error(e.getMensagem());
        restTemplate.postForObject(url, new ErroDTO(e, "ERROR", "418"), Object.class);
        System.err.println(e.getMensagem());
    }
}