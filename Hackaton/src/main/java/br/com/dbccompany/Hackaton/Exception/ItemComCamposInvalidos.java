package br.com.dbccompany.Hackaton.Exception;

public class ItemComCamposInvalidos extends GeneralException{
    public ItemComCamposInvalidos(){
        super("Pré item apresenta campos inválidos!");
    }
}
