package br.com.dbccompany.Hackaton.Exception;

public class ImpossivelConstruirWrapper extends GeneralException {
    public ImpossivelConstruirWrapper(){
        super("Pré Wrapper inválido!");
    }
}
