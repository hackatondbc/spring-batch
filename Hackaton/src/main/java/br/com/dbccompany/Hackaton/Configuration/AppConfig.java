package br.com.dbccompany.Hackaton.Configuration;

import br.com.dbccompany.Hackaton.IO.RawData;
import br.com.dbccompany.Hackaton.Process.TextProcessor;
import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.MultiResourceItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.Arrays;
import java.util.Date;


@EnableBatchProcessing
@EnableScheduling
@Configuration
public class AppConfig {

    @Autowired
    private ResourcePatternResolver resourcePatternResolver;

    @Autowired
    private JobBuilderFactory jobs;

    @Autowired
    private StepBuilderFactory steps;

    @Value(value = "file:${user.home}/data/in/*.dat")
    private Resource[] resources;

    private final Resource outputResource = new FileSystemResource(System.getProperty("user.home") + "/data/out/output.done.dat");

    @Autowired
    private JobLauncher jobLauncher;

    public void atualizarResources(){
        this.resources.getClass().getResource("file:${user.home}/data/in/*.dat");
    }

    @Scheduled(fixedRate = 5000)
    public void schedule() throws JobParametersInvalidException, JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException {
        jobLauncher.run(myJob(), new JobParametersBuilder().addDate("date", new Date()).toJobParameters());
    }

    @Bean
    public Job myJob() {
        return jobs
                .get("myJob")
                .incrementer(new RunIdIncrementer())
                .flow(step1())
                .end()
                .build();
    }

    @Bean
    protected Step step1() {
        return steps.get("step1")
                .<RawData, RawData> chunk(1)
                .reader(multiResourceItemReader())
                .processor(new TextProcessor())
                .writer(write())
                .build();
    }


    @Bean
    @StepScope
    protected FlatFileItemReader<RawData> itemReader(){
        FlatFileItemReader<RawData> reader = new FlatFileItemReader<>();
        reader.setLineMapper(new DefaultLineMapper<RawData>() {{
            setLineTokenizer(new DelimitedLineTokenizer() {{
                setNames("texto");
                setDelimiter(":D");
            }});
            setFieldSetMapper(new BeanWrapperFieldSetMapper<RawData>() {{
                setTargetType(RawData.class);
            }});
        }});
        return reader;
    }

    @Bean
    @StepScope
    public MultiResourceItemReader<RawData> multiResourceItemReader(){
        MultiResourceItemReader<RawData> multiResourceItemReader = new MultiResourceItemReader<RawData>();
        System.out.println(new FileSystemResource(System.getProperty("user.home") + "/data/in/*.dat"));
        this.atualizarResources();
        multiResourceItemReader.setResources(resources);
        System.out.println(Arrays.toString(resources));
        multiResourceItemReader.setDelegate(itemReader());
        return multiResourceItemReader;
    }

    @Bean
    @StepScope
    protected FlatFileItemWriter<RawData> write(){
        FlatFileItemWriter<RawData> writer = new FlatFileItemWriter<RawData>();
        writer.setResource(outputResource);
        DelimitedLineAggregator<RawData> lineAggregator = new DelimitedLineAggregator<RawData>();
        lineAggregator.setDelimiter(",");

        BeanWrapperFieldExtractor<RawData> fieldExtractor = new BeanWrapperFieldExtractor<RawData>();
        fieldExtractor.setNames(new String[]{"texto"});
        lineAggregator.setFieldExtractor(fieldExtractor);
        writer.setLineAggregator(lineAggregator);
        return writer;
    }
}