package br.com.dbccompany.Hackaton.Util;

import br.com.dbccompany.Hackaton.DTO.ItemDTO;
import br.com.dbccompany.Hackaton.DTO.VendaDTO;
import br.com.dbccompany.Hackaton.DTO.VendedorDTO;
import br.com.dbccompany.Hackaton.DTO.WrapperDTO;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;

public class AnaliseDeDados {
    private WrapperDTO dados;

    public AnaliseDeDados(WrapperDTO dados) {
        this.dados = dados;
    }

    public AnaliseDeDados(){}

    public String quantidadeClientes(){
        int quantidadeCliente = this.dados.getClientes().size();
        return Integer.toString(quantidadeCliente);
    }

    public String quantidadeVendedores(){
        int quantidadeVendedores = this.dados.getVendedores().size();
        return Integer.toString(quantidadeVendedores);
    }

    public String idVendaMaisCara(){
        List<VendaDTO> vendas = this.dados.getVendas();
        Double valorMaiorVenda = 0.0;
        String idMaiorVenda = "";
        for(VendaDTO venda : vendas ){
            List<ItemDTO> itensVenda = venda.getItens();
            Double total = valorTotalVenda(itensVenda);
            if( total > valorMaiorVenda ){
                valorMaiorVenda = total;
                idMaiorVenda = venda.getId();
            }
        }
        return idMaiorVenda;
    }

    public String  piorVendedor(){
        VendedorDTO piorVendedor = new VendedorDTO();
        double piorValor = 9999999999.00;
        for( VendedorDTO vendedor : this.dados.getVendedores() ){
            List<VendaDTO> vendasVendedor = this.dados.procurarVendasDeUmVendedor(vendedor.getNome());
            Double total = 0.0;
            for( VendaDTO venda : vendasVendedor ){
                total += valorTotalVenda(venda.getItens());
            }
            if(total < piorValor ){
                piorValor = total;
                piorVendedor = vendedor;
            }
        }
        return piorVendedor.toString();
    }


    public Double valorTotalVenda( List<ItemDTO> itensVenda ){
        double total = 0.0;
        for( ItemDTO item : itensVenda ){
            total += Double.parseDouble(item.getPreco()) * Double.parseDouble( item.getQuantidade() );
        }
        return total;
    }

}
