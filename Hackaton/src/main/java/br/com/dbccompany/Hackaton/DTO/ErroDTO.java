package br.com.dbccompany.Hackaton.DTO;


import br.com.dbccompany.Hackaton.Exception.GeneralException;

import java.time.LocalDateTime;

public class ErroDTO {
    private String data;
    private String tipo;
    private String codigo;
    private String descricao;

    public ErroDTO(GeneralException e, String tipo, String codigo) {
        this.data = LocalDateTime.now().toString();
        this.tipo = tipo;
        this.codigo = codigo;
        this.descricao = "Causa: " + e.getMensagem();
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
