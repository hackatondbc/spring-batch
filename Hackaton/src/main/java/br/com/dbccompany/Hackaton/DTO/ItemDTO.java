package br.com.dbccompany.Hackaton.DTO;

import javax.persistence.criteria.CriteriaBuilder;

public class ItemDTO {
    private String id;
    private String preco;
    private String quantidade;

    public ItemDTO(String id, String preco, String quantidade) {
        this.id = id;
        this.preco = preco;
        this.quantidade = quantidade;
    }

    public ItemDTO() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPreco() {
        return preco;
    }

    public void setPreco(String preco) {
        this.preco = preco;
    }

    public String getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(String quantidade) {
        this.quantidade = quantidade;
    }
}
