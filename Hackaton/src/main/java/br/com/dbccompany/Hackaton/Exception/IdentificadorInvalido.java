package br.com.dbccompany.Hackaton.Exception;

public class IdentificadorInvalido extends GeneralException {
    public IdentificadorInvalido(){
        super("Pré Wrapper possui identificadores inválidos!");
    }
}
