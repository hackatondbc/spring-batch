package br.com.dbccompany.Hackaton.Process;

import br.com.dbccompany.Hackaton.DTO.*;
import br.com.dbccompany.Hackaton.Exception.*;
import br.com.dbccompany.Hackaton.IO.RawData;
import br.com.dbccompany.Hackaton.Util.AnaliseDeDados;
import br.com.dbccompany.Hackaton.Util.LogsGenerator;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ProcessorService {



    public RawData processadorDeArquivos (RawData input ) throws ImpossivelConstruirWrapper {
        try{
            List<String> preWrapper = dividirPorEspacoEZerosEmArray( input );
            if( preWrapper != null ){
                return analisarDados( construirWrapper( preWrapper ) );
            }
            throw new ImpossivelConstruirWrapper();
        } catch(GeneralException e ){
            LogsGenerator.erro( e );
            return null;
        }
    }

    public WrapperDTO construirWrapper (List<String> preWrapper) throws IdentificadorInvalido {
        try{
            WrapperDTO wrapper = new WrapperDTO();
            for( String linha : preWrapper ){
                char id = linha.charAt(0);
                if( id == '1' ){
                    VendedorDTO vendedor = construirVendedor( linha );
                    wrapper.adicionarVendedor(vendedor);
                }else if( id == '2' ){
                    wrapper.adicionarCliente(construirCliente( linha ));
                }else if( id == '3' ){
                    wrapper.adicionarVendas(construirVenda( linha, wrapper ));
                } else{
                    throw new IdentificadorInvalido();
                }
            }
            return wrapper;
        } catch (GeneralException e){
            LogsGenerator.erro( e );
            return null;
        }
    }

    public RawData analisarDados( WrapperDTO objetoFinal ) throws DadosAnalisadosInvalidos {
        try{
            AnaliseDeDados analise = new AnaliseDeDados(objetoFinal);
            if( analise.quantidadeClientes() != null
                    && analise.quantidadeVendedores() != null
                    && analise.idVendaMaisCara() != null
                    && analise.piorVendedor() != null ) {

                String string = "Quantidade Cliente: " +
                        analise.quantidadeClientes() +
                        "\n Quantidade Vendedores: " +
                        analise.quantidadeVendedores() +
                        "\n ID da venda mais cara: " +
                        analise.idVendaMaisCara() +
                        "\n Pior Vendedor: " +
                        analise.piorVendedor() + "\n";
                return new RawData(string);
            }
            throw new DadosAnalisadosInvalidos();
        } catch (GeneralException e){
            LogsGenerator.erro( e );
            return null;
        }
    }

    public VendaDTO construirVenda( String dados, WrapperDTO wrapper ) throws VendaComCamposInvalidos {
        try{
            List<String> preVenda = converterStringEmList(dados, "ç");
            VendaDTO venda = new VendaDTO();
            if( preVenda.get(1) != null && preVenda.get(2) != null && preVenda.get(3) != null ){
                venda.setId(preVenda.get(1));
                List<ItemDTO> itens = construirItens(preVenda.get(2));
                venda.setItens(itens);
                VendedorDTO vendedor = wrapper.procurarVendedorPorNome( preVenda.get(3) );
                if( vendedor != null ){
                    venda.setVendedor(vendedor);
                    return venda;
                }
            }

            throw new VendaComCamposInvalidos();
        } catch ( GeneralException e ){
            LogsGenerator.erro( e );
            return null;
        }
    }

    public List<ItemDTO> construirItens( String itens ) throws ItemComCamposInvalidos {
        try{
            String itensSemColchetes = itens.substring(1, itens.length()-1);
            List<ItemDTO> arrayItens = new ArrayList<>();
            List<String> arrayPreItens = converterStringEmList( itensSemColchetes, "," );
            for( String preItem : arrayPreItens ){
                List<String> camposItem = converterStringEmList( preItem, "-" );
                if( camposItem.get(0) != null && camposItem.get(1) != null && camposItem.get(2) != null ){
                    ItemDTO item = new ItemDTO();
                    item.setId(camposItem.get(0));
                    item.setQuantidade(camposItem.get(1));
                    item.setPreco(camposItem.get(2));
                    arrayItens.add(item);
                } else{
                    throw new ItemComCamposInvalidos();
                }
            }
            return arrayItens;
        } catch ( GeneralException e ){
            LogsGenerator.erro( e );
            return null;
        }
    }

    public ClienteDTO construirCliente (String dados) throws ClienteComCamposInvalidos {
        List<String> preCliente = converterStringEmList(dados, "ç");
        ClienteDTO cliente = new ClienteDTO();
        if( preCliente.get(1) != null && preCliente.get(2) != null && preCliente.get(3) != null){
            cliente.setCnpj(preCliente.get(1));
            cliente.setNome(preCliente.get(2));
            cliente.setAreaNegocio(preCliente.get(3));
            return cliente;
        }
        throw new ClienteComCamposInvalidos();
    }

    public VendedorDTO construirVendedor(String dados) throws VendedorComCamposInvalidos {
        List<String> preVendedor = converterStringEmList(dados, "ç");
        VendedorDTO vendedor = new VendedorDTO();
        if( preVendedor.get(1) != null && preVendedor.get(2) != null && preVendedor.get(3) != null ){
            vendedor.setCpf(preVendedor.get(1));
            vendedor.setNome(preVendedor.get(2));
            vendedor.setSalario(preVendedor.get(3));
            return vendedor;
        }
        throw new VendedorComCamposInvalidos();
    }

    public List<String> dividirPorEspacoEZerosEmArray( RawData texto ){
        List<String> linhas = converterStringEmList(texto.getTexto(), " 00");
        linhas.set(0, linhas.get(0).substring(2));
        return linhas;
    }

    public List<String> converterStringEmList(String dado, String identificador){
        return new ArrayList<>(Arrays.asList(dado.split(identificador)));
    }
}
