package br.com.dbccompany.Hackaton.Exception;

public class VendedorComCamposInvalidos extends GeneralException {
    public VendedorComCamposInvalidos(){
        super("Pré vendedor com campos inválidos!");
    }
}
