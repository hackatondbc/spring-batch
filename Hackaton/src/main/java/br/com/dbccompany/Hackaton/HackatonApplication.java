package br.com.dbccompany.Hackaton;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@EnableScheduling
@EnableBatchProcessing
@SpringBootApplication
@Configuration
public class HackatonApplication {

	public static void main( String[] args) {
		SpringApplication.run(HackatonApplication.class, args);
	}

}
