package br.com.dbccompany.Hackaton.DTO;

import java.util.ArrayList;
import java.util.List;

public class VendaDTO {
    private String id;
    private List<ItemDTO> itens = new ArrayList<>();
    private VendedorDTO vendedor;

    public VendaDTO(String id, List<ItemDTO> itens, VendedorDTO vendedor) {
        this.id = id;
        this.itens = itens;
        this.vendedor = vendedor;
    }

    public VendaDTO() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<ItemDTO> getItens() {
        return itens;
    }

    public void setItens(List<ItemDTO> itens) {
        this.itens = itens;
    }

    public VendedorDTO getVendedor() {
        return vendedor;
    }

    public void setVendedor(VendedorDTO vendedor) {
        this.vendedor = vendedor;
    }
}
