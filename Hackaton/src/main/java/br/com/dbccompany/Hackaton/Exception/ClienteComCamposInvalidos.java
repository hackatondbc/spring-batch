package br.com.dbccompany.Hackaton.Exception;

public class ClienteComCamposInvalidos extends GeneralException{
    public ClienteComCamposInvalidos(){
        super("Pré cliente apresenta campos inválidos!");
    }
}
