package br.com.dbccompany.HackatonLog;

import br.com.dbccompany.HackatonLog.Entity.LogEntity;
import br.com.dbccompany.HackatonLog.Repository.LogRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;

import java.util.List;

@DataMongoTest
public class LogRepositoryTest {

    @Autowired
    private LogRepository rep;

    @Test
    public void salvarLog(){
        LogEntity log = new LogEntity();
        log.setTipo("WARN");
        log.setCodigo("304");
        log.setDescricao("Mensagem de teste do Log.");
        rep.save(log);
        List<LogEntity> logs = rep.findAllByCodigo(log.getCodigo());

        Assertions.assertEquals(log.getCodigo(), logs.get(0).getCodigo());
    }
}
