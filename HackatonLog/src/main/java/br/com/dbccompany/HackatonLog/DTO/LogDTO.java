package br.com.dbccompany.HackatonLog.DTO;

import br.com.dbccompany.HackatonLog.Entity.LogEntity;

public class LogDTO {
    private String data;
    private String tipo;
    private String codigo;
    private String descricao;

    public LogDTO(){}

    public LogDTO(LogEntity log){
        this.data = log.getData();
        this.tipo = log.getTipo();
        this.codigo = log.getCodigo();
        this.descricao = log.getDescricao();
    }

    public LogEntity converter(){
        LogEntity log = new LogEntity();
        log.setData( this.data );
        log.setTipo( this.tipo );
        log.setCodigo( this.codigo );
        log.setDescricao( this.descricao );
        return log;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
