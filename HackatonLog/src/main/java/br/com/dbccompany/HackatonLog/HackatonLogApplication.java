package br.com.dbccompany.HackatonLog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HackatonLogApplication {

	public static void main(String[] args) {
		SpringApplication.run(HackatonLogApplication.class, args);
	}

}
