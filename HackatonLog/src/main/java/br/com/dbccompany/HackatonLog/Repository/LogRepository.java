package br.com.dbccompany.HackatonLog.Repository;

import br.com.dbccompany.HackatonLog.Entity.LogEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LogRepository extends MongoRepository<LogEntity, String> {

    List<LogEntity> findAll();
    List<LogEntity> findAllById(String id);
    List<LogEntity> findAllByCodigo(String codigo);
    List<LogEntity> findAllByDescricao(String mensagem);
    List<LogEntity> findAllByCodigoAndDescricao(String codigo, String mensagem);


}