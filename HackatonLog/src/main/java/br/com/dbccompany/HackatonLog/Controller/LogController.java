package br.com.dbccompany.HackatonLog.Controller;

import br.com.dbccompany.HackatonLog.DTO.LogDTO;
import br.com.dbccompany.HackatonLog.Service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping( value = "/hackatonLogs" )
public class LogController {

    @Autowired
    private LogService service;

    @GetMapping(path = "/")
    public List<LogDTO> retornarTodos() {
        return service.findAll();
    }

    @PostMapping(path = "/salvar")
    public LogDTO salvar(@RequestBody LogDTO erro) {
        return service.save(erro);
    }

    @GetMapping(path = "/buscarPorCodigo/{codigo}")
    public List<LogDTO> buscarPorCodigo(@PathVariable String codigo) {
        return service.findAllByCodigo(codigo);
    }
}
